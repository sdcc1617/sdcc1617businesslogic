package com.sdcc.business.logic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SdccBusinessLogicApplication {

	public static void main(String[] args) {
		SpringApplication.run(SdccBusinessLogicApplication.class, args);
	}
}
