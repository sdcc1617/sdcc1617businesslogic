package com.sdcc.business.logic.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sdcc.business.logic.controller.UserService;
import com.sdcc.business.logic.model.rest.DTOUser;
import com.sdcc.business.logic.model.rest.response.DTOResponseUser;

@RestController
@RequestMapping("/user")
public class RestPresentation {
	
	/** The strategy service. */
	@Autowired
	UserService userService;

	/**
	 * Questo metodo deve essere richiesto al bus!
	 * Ma non faccio la modifica qui, ma in strategyService
	 * */
	
	@RequestMapping(value = "/getHello", method = RequestMethod.GET)
	public ResponseEntity<String> getHello() {

		return new ResponseEntity<String>("Hello", HttpStatus.OK);

	}
	
	@RequestMapping(value = "/createUser", method = RequestMethod.POST)
	public ResponseEntity<?> createStrategy(@RequestBody DTOUser dtoUser) {

		return userService.createUser(dtoUser);

	}
	
	@RequestMapping(value = "/getUsers", method = RequestMethod.GET)
	public ResponseEntity<?> getUsers() {

		return userService.getUsers();

	}
	
	@RequestMapping(value = "/getUsersByRole", method = RequestMethod.GET)
	public ResponseEntity<?> getUsersByRole(
			@RequestParam("role") String role) {

		return userService.getUsersByRole(role);

	}
}
