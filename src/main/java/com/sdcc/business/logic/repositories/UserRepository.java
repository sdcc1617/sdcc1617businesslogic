package com.sdcc.business.logic.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sdcc.business.logic.model.User;
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	Iterable<User> findByRole(String role);

}