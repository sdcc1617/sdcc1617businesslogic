package com.sdcc.business.logic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class SdccBusinessLogic1617Application extends SpringBootServletInitializer {

	public static void main(String[] args){
		SpringApplication.run(SdccBusinessLogic1617Application.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	    return application.sources(applicationClass);
	}

	private static Class<SdccBusinessLogic1617Application> applicationClass = SdccBusinessLogic1617Application.class;
}
