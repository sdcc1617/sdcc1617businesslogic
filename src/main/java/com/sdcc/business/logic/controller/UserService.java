package com.sdcc.business.logic.controller;

import org.springframework.http.ResponseEntity;

import com.sdcc.business.logic.model.rest.DTOUser;
import com.sdcc.business.logic.model.rest.response.DTOResponseUser;

public interface UserService {

	ResponseEntity<?> createUser(DTOUser dtoUser);
	ResponseEntity<?> getUsers();
	ResponseEntity<?> getUsersByRole(String role);

}
