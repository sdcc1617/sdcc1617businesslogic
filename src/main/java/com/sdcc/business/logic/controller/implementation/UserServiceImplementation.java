package com.sdcc.business.logic.controller.implementation;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.sdcc.business.logic.controller.UserService;
import com.sdcc.business.logic.model.User;
import com.sdcc.business.logic.model.rest.DTOUser;
import com.sdcc.business.logic.model.rest.response.DTOResponseUser;
import com.sdcc.business.logic.repositories.UserRepository;

@Service("UserService")
public class UserServiceImplementation implements UserService{
	@Autowired
	UserRepository userRepository;
	
	@Override
	public  ResponseEntity<?>  createUser(DTOUser dtoUser){
		
		User user = new User(dtoUser.getName(), 
							dtoUser.getSurname(),
							dtoUser.getPassword(),
							dtoUser.getEmail(),
							dtoUser.getRole());
		userRepository.save(user);
		DTOResponseUser dtoResponse = new DTOResponseUser();
		dtoResponse.setId(user.getId());
		dtoResponse.setName(user.getName());
		dtoResponse.setSurname(user.getSurname());
		dtoResponse.setEmail(user.getEmail());
		dtoResponse.setPassword(user.getPassword());
		dtoResponse.setRole(user.getRole());
		dtoResponse.setCreated(user.getCreated());
		dtoResponse.setUpdated(user.getUpdated());
		
		ResponseEntity<DTOResponseUser> responseEntity = 
				new ResponseEntity<DTOResponseUser>(dtoResponse,HttpStatus.CREATED);
		return responseEntity;
		
	}

	@Override
	public ResponseEntity<?> getUsers() {
		Iterable<User> users = userRepository.findAll();
		List<DTOResponseUser> dtoResponseUserList = new LinkedList<DTOResponseUser>();
		for(User user : users){
			DTOResponseUser dtoResponseUser = new DTOResponseUser();
			dtoResponseUser.setId(user.getId());
			dtoResponseUser.setName(user.getName());
			dtoResponseUser.setSurname(user.getSurname());
			dtoResponseUser.setEmail(user.getEmail());
			dtoResponseUser.setPassword(user.getPassword());
			dtoResponseUser.setRole(user.getRole());
			dtoResponseUser.setCreated(user.getCreated());
			dtoResponseUser.setUpdated(user.getUpdated());
			dtoResponseUserList.add(dtoResponseUser);
		}
		ResponseEntity<List<DTOResponseUser>> responseEntity = 
				new ResponseEntity<List<DTOResponseUser>>(dtoResponseUserList,HttpStatus.OK);
		return responseEntity;
	}
	
	@Override
	public ResponseEntity<?> getUsersByRole(String role){
		Iterable<User> users = userRepository.findByRole(role);
		List<DTOResponseUser> dtoResponseUserList = new LinkedList<DTOResponseUser>();
		for(User user : users){
			DTOResponseUser dtoResponseUser = new DTOResponseUser();
			dtoResponseUser.setId(user.getId());
			dtoResponseUser.setName(user.getName());
			dtoResponseUser.setSurname(user.getSurname());
			dtoResponseUser.setEmail(user.getEmail());
			dtoResponseUser.setPassword(user.getPassword());
			dtoResponseUser.setRole(user.getRole());
			dtoResponseUser.setCreated(user.getCreated());
			dtoResponseUser.setUpdated(user.getUpdated());
			dtoResponseUserList.add(dtoResponseUser);
		}
		ResponseEntity<List<DTOResponseUser>> responseEntity = 
				new ResponseEntity<List<DTOResponseUser>>(dtoResponseUserList,HttpStatus.OK);
		return responseEntity;
	}

}
